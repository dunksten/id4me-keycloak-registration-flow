package org.id4me.keycloak.registrationFlow.message;

public class ID4meMessages {
    public static final String MISSING_IDENTIFIER = "missingIdentifierMessage";
    public static final String IDENTIFIER_EXISTS = "identifierExistsMessage";
    public static final String INVALID_IDENTIFIER = "invalidIdentifierMessage";

    public ID4meMessages () {

    }
}
