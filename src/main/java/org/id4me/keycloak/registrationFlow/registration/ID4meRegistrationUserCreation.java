package org.id4me.keycloak.registrationFlow.registration;

import org.id4me.keycloak.registrationFlow.message.ID4meMessages;
import org.keycloak.Config;
import org.keycloak.authentication.FormAction;
import org.keycloak.authentication.FormActionFactory;
import org.keycloak.authentication.FormContext;
import org.keycloak.authentication.ValidationContext;
import org.keycloak.events.Details;
import org.keycloak.events.Errors;
import org.keycloak.events.EventType;
import org.keycloak.forms.login.LoginFormsProvider;
import org.keycloak.models.*;
import org.keycloak.models.utils.FormMessage;
import org.keycloak.protocol.oidc.OIDCLoginProtocol;
import org.keycloak.provider.ProviderConfigProperty;
import org.keycloak.services.resources.AttributeFormDataProcessor;
import org.keycloak.services.validation.Validation;

import javax.ws.rs.core.MultivaluedMap;
import java.util.ArrayList;
import java.util.List;

public class ID4meRegistrationUserCreation implements FormAction, FormActionFactory {

    public static final String PROVIDER_ID = "id4me-registration-user-creation";

    @Override
    public String getHelpText() {
        return "This action must always be first! Validates the username of the user in validation phase.  In success phase, this will create the user in the database.";
    }

    @Override
    public List<ProviderConfigProperty> getConfigProperties() {
        return null;
    }

    @Override
    public void validate(ValidationContext context) {
        MultivaluedMap<String, String> formData = context.getHttpRequest().getDecodedFormParameters();
        List<FormMessage> errors = new ArrayList<>();
        context.getEvent().detail(Details.REGISTER_METHOD, "form");

        String localPart = formData.getFirst(ID4meRegistrationPage.FIELD_IDENTIFIER);
        String domainPart = formData.getFirst(ID4meRegistrationPage.FIELD_DOMAIN);
        String identifier = localPart + ID4meRegistrationPage.AT + domainPart;
        context.getEvent().detail(Details.USERNAME, identifier);

        String identifierField = ID4meRegistrationPage.FIELD_IDENTIFIER;
        if (Validation.isBlank(identifier)) {
            context.error(Errors.INVALID_REGISTRATION);
            errors.add(new FormMessage(ID4meRegistrationPage.FIELD_IDENTIFIER, ID4meMessages.MISSING_IDENTIFIER));
            context.validationError(formData, errors);
            return;
        }

        if (!Validation.isEmailValid(identifier)) {
            context.error(Errors.INVALID_REGISTRATION);
            errors.add(new FormMessage(ID4meRegistrationPage.FIELD_IDENTIFIER, ID4meMessages.INVALID_IDENTIFIER));
            context.validationError(formData, errors);
            return;
        }

        if (context.getSession().users().getUserByUsername(identifier, context.getRealm()) != null) {
            context.error(Errors.USERNAME_IN_USE);
            errors.add(new FormMessage(identifierField, ID4meMessages.IDENTIFIER_EXISTS));
            formData.remove(ID4meRegistrationPage.FIELD_IDENTIFIER);
            context.validationError(formData, errors);
            return;
        }

        context.success();
    }

    @Override
    public void buildPage(FormContext context, LoginFormsProvider form) {
        ArrayList<String> domainList = new ArrayList<>();
        domainList.add("web.de");
        domainList.add("gmx.de");
        form.setAttribute("domainList", domainList);
    }

    @Override
    public void success(FormContext context) {
        MultivaluedMap<String, String> formData = context.getHttpRequest().getDecodedFormParameters();
        String localPart = formData.getFirst(ID4meRegistrationPage.FIELD_IDENTIFIER);
        String domainPart = formData.getFirst(ID4meRegistrationPage.FIELD_DOMAIN);
        String identifier = localPart + ID4meRegistrationPage.AT + domainPart;

        context.getEvent().detail(Details.USERNAME, identifier)
                .detail(Details.REGISTER_METHOD, "form")
        ;
        UserModel user = context.getSession().users().addUser(context.getRealm(), identifier);
        user.setEnabled(true);

        context.getAuthenticationSession().setClientNote(OIDCLoginProtocol.LOGIN_HINT_PARAM, identifier);
        AttributeFormDataProcessor.process(formData, context.getRealm(), user);
        context.setUser(user);
        context.getEvent().user(user);
        context.getEvent().success();
        context.newEvent().event(EventType.LOGIN);
        context.getEvent().client(context.getAuthenticationSession().getClient().getClientId())
                .detail(Details.REDIRECT_URI, context.getAuthenticationSession().getRedirectUri())
                .detail(Details.AUTH_METHOD, context.getAuthenticationSession().getProtocol());
        String authType = context.getAuthenticationSession().getAuthNote(Details.AUTH_TYPE);
        if (authType != null) {
            context.getEvent().detail(Details.AUTH_TYPE, authType);
        }
    }

    @Override
    public boolean requiresUser() {
        return false;
    }

    @Override
    public boolean configuredFor(KeycloakSession session, RealmModel realm, UserModel user) {
        return true;
    }

    @Override
    public void setRequiredActions(KeycloakSession session, RealmModel realm, UserModel user) {

    }

    @Override
    public boolean isUserSetupAllowed() {
        return false;
    }


    @Override
    public void close() {

    }

    @Override
    public String getDisplayType() {
        return "ID4me Registration User Creation";
    }

    @Override
    public String getReferenceCategory() {
        return null;
    }

    @Override
    public boolean isConfigurable() {
        return false;
    }

    private static AuthenticationExecutionModel.Requirement[] REQUIREMENT_CHOICES = {
            AuthenticationExecutionModel.Requirement.REQUIRED,
            AuthenticationExecutionModel.Requirement.DISABLED
    };
    @Override
    public AuthenticationExecutionModel.Requirement[] getRequirementChoices() {
        return REQUIREMENT_CHOICES;
    }
    @Override
    public FormAction create(KeycloakSession session) {
        return this;
    }

    @Override
    public void init(Config.Scope config) {

    }

    @Override
    public void postInit(KeycloakSessionFactory factory) {

    }

    @Override
    public String getId() {
        return PROVIDER_ID;
    }
}
