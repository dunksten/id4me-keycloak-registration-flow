ID4me Keycloak Registration Flow
===================================================

### Prerequisites
- Java Development Kit 8
- Maven 3.3+

#### Standalone
1. First, Keycloak must be running. See [Getting Started](https://github.com/keycloak/keycloak#getting-started), or you
   can build distribution from [source](https://github.com/keycloak/keycloak/blob/master/docs/building.md).

2. Execute the follow.  This will build the module.

   `$ mvn package`
   
3. Copy the jar from /target to KEYCLOAK_HOME/standalone/deployments.

4. Login to admin console.  Hit browser refresh if you are already logged in so that the new providers show up.

5. Go to the **Authentication** menu item and go to the **Flows** tab, you will be able to view the currently
   defined flows.  You cannot modify an built in flows, so, to add the Authenticator you
   have to copy an existing flow or create your own.  Create an **ID4me_Registration_Flow**.

6. In your copy, click the **Actions** menu item in **Forms** subflow and **Add Execution**.  Pick `ID4me Registration User Creation` and change 
   the **Requirement** choice. After that, do the same with `ID4me Password Validation` and `ID4me Profile Validation`.
   Instead of **REQUIRED** set these to **DISABLED**.
   
7. Go to the **Bindings** tab in **Authentication** menu and change the default **Registration Flow** to your registration flow 
   and click `Save`.
   
#### Docker
1. Build the module.

2. Start the Keycloak instance with the provided `docker-compose.yml`.

3. Every time the module is rebuilt, Keycloak will find the updated version and hot reload the deployment.

4. Follow the other steps above to activate the module.

